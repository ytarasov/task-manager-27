package ru.t1.ytarasov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER TASK ID");
        @Nullable final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final String userId = getAuthService().getUserId();
        getProjectTaskService().bindTaskToProject(userId, taskId, projectId);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }
}
